<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\produk;
use App\Models\jenis_produk;
use App\Models\min_pemesanan;

class min_pemesanan extends Model
{
    use HasFactory;

    protected $table = 'pemesanan';

    protected $fillable = ['min_pemesanan'];

    public function produk(){
        return $this->hasOne(produk::class,'min_pemesanan_id');
    }
}

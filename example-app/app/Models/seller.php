<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\produk;
use App\Models\jenis_produk;
use App\Models\min_pemesanan;

class seller extends Model
{
    use HasFactory;

    protected $table = 'seller';

    protected $fillable = ['nama_seller'];

    
    // public function produk(){
    //     return $this->hasOne(produk::class,'jenis_produk_id');
    // }
}


<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\produk;
use App\Models\jenis_produk;
use App\Models\min_pemesanan;

class jenis_produk extends Model
{
    use HasFactory;

    protected $table = 'jenis_produk';

    protected $fillable = ['jenis_poduk'];

    
    public function produk(){
        return $this->hasOne(produk::class,'jenis_produk_id');
    }
}


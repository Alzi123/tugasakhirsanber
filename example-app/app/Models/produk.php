<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\produk;
use App\Models\jenis_produk;
use App\Models\min_pemesanan;

class produk extends Model
{
    use HasFactory;

    protected $table = 'produk';

    protected $fillable = ['nama_produk','harga','detail_produk','jenis_produk_id','min_pemesanan_id','image'];

    public function jenis_produk(){
        return $this->belongsTo(jenis_produk::class,'jenis_produk_id');

    }

    public function min_pemesanan(){
        return $this->belongsTo(min_pemesanan::class,'min_pemesanan_id');

    }
}



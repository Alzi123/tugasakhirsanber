<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\produk;
use App\Models\jenis_produk;
use App\Models\min_pemesanan;

class transaksi extends Model
{
    use HasFactory;

    protected $table = 'transaksi';

    protected $fillable = ['nama_barang','pembayaran','status','harga','seller','customer_name','customer_number','customer_email'];

    
    // public function produk(){
    //     return $this->hasOne(produk::class,'jenis_produk_id');
    // }
}


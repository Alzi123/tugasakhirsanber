<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\produk;

class tampilprodukcontroller extends Controller
{
    public function tampil(){
        $produk = produk::all();

        return view('produk.tampil',['produk' => $produk]); 
    }
}


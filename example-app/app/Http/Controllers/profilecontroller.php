<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\profile;
use Illuminate\Support\Facades\Auth;



class profilecontroller extends Controller
{
   
    public function index()
    {        
        $iduser = Auth::id();

        $detailprofile = profile::where('user_id', $iduser)->first();

        return view('profile.home',['detailprofile' => $detailprofile]);
    }

    public function update(Request $request, $id)
    {
        // dd($request->all());

        $request->validate([
            'bio' => 'required',
            'age' => 'required'
        ]); 
            
        $profile = profile::find($id);

        $profile->bio = $request->bio;
        $profile->age = $request->age;

        $profile->save();

        return redirect('/profile');
    }


}

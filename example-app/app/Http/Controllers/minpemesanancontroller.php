<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\produk;
use App\Models\jenis_produk;
use App\Models\min_pemesanan;
use File;

class minpemesanancontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $min_pemesanan = min_pemesanan::all();

        return view('pemesanan.home',['min_pemesanan' => $min_pemesanan]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $min_pemesanan = min_pemesanan::all();

        return view('pemesanan.tambah',['min_pemesanan' => $min_pemesanan]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'min_pemesanan' => 'required',
        ]); 
            
        $min_pemesanan = new min_pemesanan;

        $min_pemesanan->min_pemesanan = $request->input('min_pemesanan');

        $min_pemesanan->save();

        return redirect('/minpemesanan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $min_pemesanan = min_pemesanan::find($id);

        return view('pemesanan.detail',['min_pemesanan' => $min_pemesanan ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $min_pemesanan = min_pemesanan::find($id);

        return view('pemesanan.edit',['min_pemesanan' => $min_pemesanan]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'min_pemesanan' => 'required',
        ]); 
            
        $min_pemesanan = min_pemesanan::find($id);

        $min_pemesanan->min_pemesanan = $request->input('min_pemesanan');

        $min_pemesanan->save();

        return redirect('/minpemesanan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $min_pemesanan = min_pemesanan::find($id);

        $min_pemesanan->delete();

        return redirect('/minpemesanan');
    }
}

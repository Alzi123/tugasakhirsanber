<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\produk;
use notify;

class excelcontroller extends Controller
{
    public function export() 
    {
       
        return Excel::download(new UsersExport, 'transaksi.xlsx');
    }
}


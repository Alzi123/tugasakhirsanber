<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\produk;
use App\Models\jenis_produk;
use App\Models\min_pemesanan;
use App\Models\transaksi;
use App\Models\seller;
use App\Models\User;
use File;

class katalogcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produk = produk::all();

        return view('katalog.home',['produk' => $produk]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $produk = produk::find($id);
        $jenis_produk = jenis_produk::all();
        $min_pemesanan = min_pemesanan::all();

        return view('katalog.tambah',['produk' => $produk,'jenis_produk' => $jenis_produk , 'min_pemesanan' => $min_pemesanan]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {
    //     //
    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produk = produk::find($id);
        $jenis_produk = jenis_produk::all();
        $min_pemesanan = min_pemesanan::all();
        $seller = seller::all();
        $User = User::all();

        return view('katalog.edit',['produk' => $produk,'jenis_produk' => $jenis_produk , 'min_pemesanan' => $min_pemesanan,'User' => $User]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produk = produk::find($id);
        $jenis_produk = jenis_produk::all();
        $min_pemesanan = min_pemesanan::all();
        $transaksi = transaksi::all();
        $seller = seller::all();
        $User = User::all();

        return view('katalog.edit',['produk' => $produk,'jenis_produk' => $jenis_produk , 'min_pemesanan' => $min_pemesanan,'transaksi' => $transaksi,'User' => $User]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'customer_name' => 'required',
            'customer_number' => 'required',
            'customer_email' => 'required',
            'seller' => 'required',
            'harga' => 'required',
            'nama_barang' => 'required',
            'produk_id' => 'required',
            'status' => 'required',
            'pembayaran' => 'required'
        ]);
        
        $transaksi = new transaksi;

                        
        $transaksi->nama_barang = $request->input('nama_barang');
        $transaksi->harga = $request->input('harga');
        $transaksi->produk_id = $request->input('produk_id');
        $transaksi->status = $request->input('status');        
        $transaksi->seller = $request->input('seller');
        $transaksi->customer_name = $request->input('customer_name');
        $transaksi->customer_number = $request->input('customer_number');
        $transaksi->customer_email = $request->input('customer_email');
        $transaksi->pembayaran = $request->input('pembayaran');

        $transaksi->save();

        return redirect('/katalog');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

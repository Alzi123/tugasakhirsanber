<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\produk;
use App\Models\jenis_produk;
use App\Models\min_pemesanan;
use File;

class jenisprodukcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jenis_produk = jenis_produk::all();

        return view('jenisproduk.home',['jenis_produk' => $jenis_produk]);
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jenis_produk = jenis_produk::all();
        $min_pemesanan = min_pemesanan::all();

        return view('jenisproduk.tambah',['jenis_produk' => $jenis_produk , 'min_pemesanan' => $min_pemesanan]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'jenis_produk' => 'required',
        ]); 
            
        $jenis_produk = new jenis_produk;

        $jenis_produk->jenis_produk = $request->input('jenis_produk');

        $jenis_produk->save();

        return redirect('/jenisproduk');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jenis_produk = jenis_produk::find($id);

        return view('jenisproduk.detail',['jenis_produk' => $jenis_produk ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jenis_produk = jenis_produk::find($id);

        return view('jenisproduk.edit',['jenis_produk' => $jenis_produk]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'jenis_produk' => 'required',
        ]); 
            
        $jenis_produk = jenis_produk::find($id);

        $jenis_produk->jenis_produk = $request->input('jenis_produk');

        $jenis_produk->save();

        return redirect('/jenisproduk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jenis_produk = jenis_produk::find($id);

        $jenis_produk->delete();

        return redirect('/jenisproduk');
    }
}

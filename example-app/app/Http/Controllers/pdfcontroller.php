<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\Models\transaksi;
use notify;

class pdfcontroller extends Controller
{

    public function index()
    {
        
    	$produk = produk::all();
    	return view('pdf.homepdf',['produk'=>$produk]);
    }

    public function pdf(){
        // $data = "Hay";        
        // $pdf = PDF::loadView('pdf.home',compact('data'));    
        // return $pdf->download('invoice.pdf');

        $transaksi = transaksi::all();
        
 
    	$pdf = PDF::loadview('pdf.home',['transaksi'=>$transaksi]);
        
    	return $pdf->download('Transaksi.pdf');
        
    }
}

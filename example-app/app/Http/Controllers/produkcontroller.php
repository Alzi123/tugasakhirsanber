<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\produk;
use App\Models\jenis_produk;
use App\Models\min_pemesanan;
use File;

class produkcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produk = produk::all();

        return view('page.home',['produk' => $produk]); 
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jenis_produk = jenis_produk::all();
        $min_pemesanan = min_pemesanan::all();

        return view('produk.tambah',['jenis_produk' => $jenis_produk , 'min_pemesanan' => $min_pemesanan]); 
        // return view('produk.tambah',['min_pemesanan' => $min_pemesanan]); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_produk' => 'required',
            'harga' => 'required',
            'detail_produk' => 'required',
            'jenis_produk' => 'required',
            'min_pemesanan' => 'required',
            'image' => 'required'
        ]); 

        
        $ImageName = time().'.'.$request->image->extension();  
     
        $request->image->move(public_path('image'), $ImageName);

        $produk = new produk;

        $produk->nama_produk = $request->input('nama_produk');
        $produk->harga = $request->input('harga');
        $produk->detail_produk = $request->input('detail_produk');
        $produk->jenis_produk_id = $request->input('jenis_produk');
        $produk->min_pemesanan_id = $request->input('min_pemesanan');
        $produk->image = $ImageName;

        $produk->save();

        return redirect('/produk');
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produk = produk::find($id);
        $jenis_produk = jenis_produk::all();
        $min_pemesanan = min_pemesanan::all();

        return view('page.detail',['produk' => $produk,'jenis_produk' => $jenis_produk , 'min_pemesanan' => $min_pemesanan]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produk = produk::find($id);
        $jenis_produk = jenis_produk::all();
        $min_pemesanan = min_pemesanan::all();

        return view('page.edit',['produk' => $produk,'jenis_produk' => $jenis_produk , 'min_pemesanan' => $min_pemesanan]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            // 'nama_produk' => 'required',
            // 'harga' => 'required',
            // 'detail_produk' => 'required',
            // 'jenis_produk' => 'required',
            // 'min_pemesanan' => 'required',
            // 'image' => 'required'
        ]);
        
        $produk = produk::find($id);

        if($request->has('image')){
            File::delete('image/' . $produk->image);

            $ImageName = time().'.'.$request->image->extension();  
     
            $request->image->move(public_path('image'), $ImageName);

            $produk->image = $ImageName;
            
        } 
                
        $produk->nama_produk = $request->input('nama_produk');
        $produk->harga = $request->input('harga');
        $produk->detail_produk = $request->input('detail_produk');
        $produk->jenis_produk_id = $request->input('jenis_produk');
        $produk->min_pemesanan_id = $request->input('min_pemesanan');
        // $produk->image = $ImageName;

        $produk->save();

        return redirect('/produk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produk = produk::find($id);
        File::delete('image/' . $produk->image);

        $produk->delete();

        return redirect('/produk');
    }
}

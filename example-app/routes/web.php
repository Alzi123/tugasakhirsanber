<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\produkcontroller;
use App\Http\Controllers\tampilprodukcontroller;
use App\Http\Controllers\jenisprodukcontroller;
use App\Http\Controllers\minpemesanancontroller;
use App\Http\Controllers\katalogcontroller;
use App\Http\Controllers\transaksicontroller;
use App\Http\Controllers\pdfcontroller;
use App\Http\Controllers\excelcontroller;
use App\Http\Controllers\profilecontroller;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('master');
});

Route::get('/login', function () {
    connectify('success', 'Connection Found', 'Success Message Here');
    return view('/katalog');
});

Route::group(['middleware' => ['auth']], function () {

    route::get('/produk',[tampilprodukcontroller::class,'tampil']);

    route::resource('home',produkcontroller::class);
    route::resource('jenisproduk',jenisprodukcontroller::class);
    route::resource('minpemesanan',minpemesanancontroller::class);
    
    route::resource('transaksi',transaksicontroller::class);
});

route::resource('katalog',katalogcontroller::class);
// route::resource('/pdf',pdfcontrollerok::class);


//Auth
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//PDF
// route::get('/pdf',function(){
//     $pdf = App::make('dompdf.wrapper');
//     $pdf->loadHTML('<h1>Test</h1>');
//     return $pdf->stream();
// });

route::get('/pdf',[pdfcontroller::class,'pdf']);


//Excel
route::get('/excel',[excelcontroller::class,'export']);

//Profile
route::resource('profile',profilecontroller::class)->only(['index','update']);


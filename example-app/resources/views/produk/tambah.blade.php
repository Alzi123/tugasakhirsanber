@extends('master')

@section('title')
Halaman Tambah Produk
@endsection

@section('konten')
    <form action="/home" method="post" enctype="multipart/form-data">
        @csrf
        <label>Nama Produk :</label><br>
        <input type="text" name="nama_produk" class="form-control"> <br>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <label>Harga :</label><br>
        <input type="text" name="harga" class="form-control"> <br>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <label>Min Pemesanan :</label><br>
        <select name="min_pemesanan" class="form-control"> <br>
            <option value="">--Pilih Minimal Pemesanan--</option>
            @forelse($min_pemesanan as $item1)
                <option value="{{$item1->id}}">{{$item1->min_pemesanan}}</option>
            @empty
            <option value="">Belum ada Jenis Produk</option>
            @endforelse

        </select> <br>

        <label>Jenis Produk :</label><br>
        <select name="jenis_produk" class="form-control"> <br>
            <option value="">--Pilih Jenis Produk Mie--</option>
            @forelse($jenis_produk as $item)
                <option value="{{$item->id}}">{{$item->jenis_produk}}</option>
            @empty
            <option value="">Belum ada Jenis Produk</option>
            @endforelse

        </select> <br>


        <label>Detail Produk :</label><br>
        <textarea cols="20" rows="10" name="detail_produk" class="form-control"></textarea> <br> <br>

        <label>Image :</label><br>
        <input type="file" name="image" class="form-control"> <br>
        <button type="submit" class="btn btn-primary">Submit</button>

    </form>

 @endsection
 
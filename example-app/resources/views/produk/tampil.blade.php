@extends('master')

@section('title')
Management Produk Indomie
@endsection

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('konten')
   <a href="/home/create" class="btn btn-primary btn-sm my-3" >Tambah Produk</a>

   <table class="table table-bordered table-striped" text-align="center">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama Produk</th>
      <th scope="col">Harga</th>     
      <th scope="col">Action</th>  
    </tr>
  </thead>
  <tbody>
    @forelse ($produk as $key => $value)
    <tr>
        <td><img src="{{asset('image/'. $value->image)}}" class="card-img-top" height="100px" alt="..."></td>
        <td>{{$value->nama_produk}}</td>
        <td>{{$value->harga}}</td>
        <td>
        <form action="/home/{{$value->id}}" method="POST">
          @csrf
          @method('DELETE')
            <a href="/home/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
            <a href="/home/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
            <input type="Submit" value="Delete" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda Yakin Menghapus Data?');" >
            <!-- <a href="/cast/delete/{{$value->id}}" onclick="return confirm('Apakah Anda Yakin Menghapus Data?');" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a> -->
        </form>
        </td> 
    </tr>
    @empty
    <tr>
        <td>Tidak ada data</td>
    </tr>
    @endforelse
  </tbody>
</table>

 @endsection
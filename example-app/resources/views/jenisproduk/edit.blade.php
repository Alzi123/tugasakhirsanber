@extends('master')

@section('title')
Halaman Edit Jenis Produk
@endsection

@section('konten')
    <form action="/jenisproduk/{{$jenis_produk->id}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <label>Jenis Produk :</label><br>
        <input type="text" name="jenis_produk" value="{{$jenis_produk->jenis_produk}}" class="form-control"> <br>
        
        <button type="submit" class="btn btn-primary">Submit</button> <br>

        
    </form>
    
        <a href="/produk" class="btn btn-secondary btn-sm my-3">Back</a>

 @endsection
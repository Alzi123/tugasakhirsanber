@extends('master')

@section('title')
Halaman Detail Jenis Produk
@endsection

@section('konten')
    <form action="/jenisproduk/{{$jenis_produk->id}}" method="post" enctype="multipart/form-data" disabled>
        @csrf
        <label>Jenis Produk :</label><br>
        <input type="text" value="{{$jenis_produk->jenis_produk}}" class="form-control" disabled> <br>
        
    </form>

        <a href="/jenisproduk" class="btn btn-secondary btn-sm">Back</a>

 @endsection
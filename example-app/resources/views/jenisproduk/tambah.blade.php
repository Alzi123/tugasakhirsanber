@extends('master')

@section('title')
Halaman Tambah Jenis Produk
@endsection

@section('konten')
    <form action="/jenisproduk" method="post" enctype="multipart/form-data">
        @csrf
        <label>Jenis Produk :</label><br>
        <input type="text" name="jenis_produk" class="form-control"> <br>
        @error('Jenis_produk')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

       
        <button type="submit" class="btn btn-primary">Submit</button>

    </form>

 @endsection
 
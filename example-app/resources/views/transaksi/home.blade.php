@extends('master')

@section('title')
Transaksi

@endsection

<!-- @push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush -->


@section('konten')

   <table class="table table-bordered table-striped" text-align="center">
  <thead>
    <tr>
      <!-- <th scope="col">#</th> -->
      <th scope="col">Nama Barang</th>
      <th scope="col">Harga</th>
      <th scope="col">Pembayaran</th>
      <th scope="col">Status Pemesanan</th>
      <th scope="col">Seller</th>
      <th scope="col">Nama Customer</th>
      <th scope="col">No HP Customer</th>
      <th scope="col">Email Customer</th>
      <th scope="col">Tanggal</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
  
    @forelse ($transaksi as $key => $value)
    <tr>
        <!-- <td><img src="{{asset('image/'. $value->image)}}" class="card-img-top" height="100px" alt="..."></td> -->
        <td>{{$value->nama_barang}}</td>
        <td>{{$value->harga}}</td>
        <td>{{$value->pembayaran }}</td>
        <td>{{$value->status}}</td>
        <td>{{$value->seller}}</td>
        <td>{{$value->customer_name}}</td>
        <td>{{$value->customer_number}}</td>
        <td>{{$value->customer_email}}</td>
        <td>{{$value->created_at}}</td>
        <td>
        <form action="/transaksi/{{$value->id}}" method="POST">        
          @csrf
          @method('DELETE')
            <!-- <a href="/minpemesanan/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
            <a href="/minpemesanan/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a> -->
            <input type="Submit" value="CANCEL" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda Yakin Membatalkan Orderan ini??');" >
            <!-- <a href="/cast/delete/{{$value->id}}" onclick="return confirm('Apakah Anda Yakin Menghapus Data?');" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a> -->
        </form>
        </td> 
    </tr>
    @empty
    <tr>
        <td>Tidak ada data</td>
    </tr>
    @endforelse
  </tbody>
  <a href="/pdf" class="btn btn-primary btn-sm my-2">Download PDF</a><br>
  <a href="/excel" class="btn btn-primary btn-sm my-2">Download Excel</a><br>
</table>


 @endsection
@extends('master')

@section('title')
Halaman Tambah Produk
@endsection

@section('konten')
    <form action="/home/{{$produk->id}}" method="post" enctype="multipart/form-data" disabled>
        @csrf
        <label>Nama Produk :</label><br>
        <input type="text" value="{{$produk->nama_produk}}" class="form-control" disabled> <br>

        <label>Harga :</label><br>
        <input type="text" value="{{$produk->harga}}" class="form-control" disabled> <br>

        <label>Min Pemesanan :</label><br>
        <input type="text" value="{{$produk->min_pemesanan->min_pemesanan}}" class="form-control" disabled> <br>

        <label>Jenis Produk :</label><br>
        <input type="text" value="{{$produk->jenis_produk->jenis_produk}}" class="form-control" disabled> <br>
        

        <label>Detail Produk :</label><br>
        <textarea cols="20" rows="10" name="detail_produk" class="form-control" disabled>{{$produk->detail_produk}}</textarea> <br> <br>
        
    </form>
    <label>Image :</label><br>
        <img src="{{asset('image/'. $produk->image)}}"  height="150px"  alt="..."> <br> <br> <br>

        <a href="/produk" class="btn btn-secondary btn-sm">Back</a>

 @endsection
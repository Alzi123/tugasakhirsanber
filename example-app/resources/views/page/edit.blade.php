@extends('master')

@section('title')
Halaman Edit Produk
@endsection

@section('konten')
    <form action="/home/{{$produk->id}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <label>Nama Produk :</label><br>
        <input type="text" name="nama_produk" value="{{$produk->nama_produk}}" class="form-control"> <br>

        <label>Harga :</label><br>
        <input type="text" name="harga" value="{{$produk->harga}}" class="form-control"> <br>

        <label>Min Pemesanan :</label><br>
        <select name="min_pemesanan" class="form-control"> <br>
            <option value="{{$produk->min_pemesanan_id}}">{{$produk->min_pemesanan->min_pemesanan}}</option>
            @forelse($min_pemesanan as $item1)
                @if ($item1->id == $produk->id)
                <option value="{{$item1->id}}" selected>{{$produk->min_pemesanan->min_pemesanan}}</option>
                @else
                <option value="{{$item1->id}}">{{$item1->min_pemesanan}}</option>
                @endif
            @empty
            <option value="">Belum ada Jenis Produk</option>
            @endforelse <br>
        </select> <br>

        <label>Jenis Produk :</label><br>
        <select name="jenis_produk" class="form-control"> <br>
            <option value="{{$produk->jenis_produk_id}}">{{$produk->jenis_produk->jenis_produk}}</option>
            @forelse($jenis_produk as $item)
                @if ($item->id == $produk->id)
                <option value="{{$item->id}}" selected>{{$produk->jenis_produk->jenis_produk}}</option>
                @else
                <option value="{{$item->id}}">{{$item->jenis_produk}}</option>
                @endif
            @empty
            <option value="">Belum ada Jenis Produk</option>
            @endforelse

        </select> <br>


        <label>Detail Produk :</label><br>
        <textarea cols="20" rows="10" name="detail_produk" class="form-control">{{$produk->detail_produk}}</textarea> <br> <br>

        <label>Image :</label><br>
        <img src="{{asset('image/'. $produk->image)}}"  height="150px"  alt="..."> <br> <br>
        <input type="file" name="image" class="form-control"> <br>
        <button type="submit" class="btn btn-primary">Submit</button> <br>

        
    </form>
    
        <a href="/produk" class="btn btn-secondary btn-sm my-3">Back</a>

 @endsection
@extends('master')

@section('title')
Halaman Menu Utama
@endsection

@section('konten')

<!-- <a href="/menu/create" class="btn btn-sm btn-primary">Tambah Menu</a><br> -->

<div class="row" >
    @forelse ($produk as $item)
                    <div class="col-3" ><br>
                        <div class="card">
                            <img src="{{asset('image/'. $item->image)}}" class="card-img-top" height="200px" alt="...">
                            <div class="card-body">
                              <h1 class="card-title">Rp. {{$item->harga}}</h1>
                              <p class="card-text">{{$item->nama_produk}}</p>
                              <a href="/katalog/{{$item->id}}" class="btn btn-primary btn-block">Detail</a>
                              <!-- <div class="row my-2">
                                <div class="col">
                                <a href="/menu/{{$item->id}}/edit" class="btn btn-info btn-block">Edit</a>
                                </div>
                                <div class="col">
                                    <form action="/menu/{{$item->id}}" method="POST">
                                      @method('DELETE')
                                      @csrf
                                      <input type="submit" value="DELETE" class="btn btn-danger btn-block">
                                    </form>
                                </div>

                              </div> -->
                            </div>
                          </div>
                      </div>
    @empty
    <h4>Belum ada Menu tersedia</h4>
    @endforelse
</div>

 @endsection

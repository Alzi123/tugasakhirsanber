@extends('master')

@section('title')
Detail Produk
@endsection

@section('konten')
<form action="/katalog/create" method="post" enctype="multipart/form-data">
        @csrf
        <label>Nama Barang : {{$produk->nama_barang}}</label><br>
        <input type="text" name="nama_barang" class="form-control"> <br>
        @error('min_pemesanan')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

       
        <button type="submit" class="btn btn-primary">Tambah Keranjang</button>

    </form>
 @endsection
 
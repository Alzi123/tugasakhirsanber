@extends('master')

@section('title')
Detail Produk
@endsection

@section('konten')
<form action="/katalog" method="post" enctype="multipart/form-data">
        @csrf
        <label>Nama Barang : </label><br>
        <input type="text" name="nama_barang" value="{{$produk->nama_produk}}" class="form-control" > <br>

        <label>Harga :</label><br>
        <input type="text" name="harga" value="{{$produk->harga}}" class="form-control" > <br>

        <label>Pemesanan :</label><br>
        <input type="text" name="min_pemesanan" value="{{$produk->min_pemesanan->min_pemesanan}}" class="form-control" > <br>


        <label>produk id : </label><br>
        <input type="text" name="produk_id" value="{{$produk->id}}" class="form-control" > <br>

        <label>status : {{$produk->nama_barang}}</label><br>
        <input type="text" name="status" value="PENDING" class="form-control" > <br>

        <label>Detail Produk :</label><br>
        <textarea cols="20" rows="10" name="detail_produk" class="form-control" >{{$produk->detail_produk}}</textarea> <br> <br>

        <label>Seller :</label><br>
        <select name="seller" class="form-control"> <br>
            <option >--Pilih Seller--</option>
            @forelse($User as $item1)
                <option >{{$item1->name}}</option>
            @empty
            <option >Belum ada seller</option>
            @endforelse

        </select> <br>

        <label>Pembayaran :</label><br>
        <select name="pembayaran" class="form-control"> <br> <br>
            <option >--Pilih Pembayaran--</option>
            <option >Cash</option>
            <option >Transfer</option>
            <option >Virtual Account</option>
            <option >Scan QR</option> 
        
        </select> <br>

        <label>Customer Name : </label><br>
        <input type="text" name="customer_name"  class="form-control" > <br>

        <label>Customer Number : </label><br>
        <input type="text" name="customer_number"  class="form-control" > <br>

        <label>Customer Email : </label><br>
        <input type="text" name="customer_email"  class="form-control" > <br>

        

        


        @auth
        <button type="submit" class="btn btn-primary">Tambah Keranjang</button>
        @endauth

    </form>
 @endsection
 
@extends('master')

@section('title')
Halaman Tambah Minimal Pemesanan
@endsection

@section('konten')
    <form action="/minpemesanan" method="post" enctype="multipart/form-data">
        @csrf
        <label>Minimal Pemesanan :</label><br>
        <input type="text" name="min_pemesanan" class="form-control"> <br>
        @error('min_pemesanan')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

       
        <button type="submit" class="btn btn-primary">Submit</button>

    </form>

 @endsection
 
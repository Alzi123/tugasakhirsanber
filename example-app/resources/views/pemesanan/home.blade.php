@extends('master')

@section('title')
Management Minimal Pemesanan
@endsection

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('konten')
   <a href="/minpemesanan/create" class="btn btn-primary btn-sm my-3" >Tambah Minimal Pemesanan</a>

   <table class="table table-bordered table-striped" text-align="center">
  <thead>
    <tr>
      <!-- <th scope="col">#</th> -->
      <th scope="col">(id)</th>
      <th scope="col">Minimal Pemesanan</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($min_pemesanan as $key => $value)
    <tr>
        <!-- <td><img src="{{asset('image/'. $value->image)}}" class="card-img-top" height="100px" alt="..."></td> -->
        <td>{{$value->id}}</td>
        <td>{{$value->min_pemesanan}}</td>
        <td>
        <form action="/minpemesanan/{{$value->id}}" method="POST">
          @csrf
          @method('DELETE')
            <a href="/minpemesanan/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
            <a href="/minpemesanan/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
            <input type="Submit" value="Delete" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda Yakin Menghapus Data?');" >
            <!-- <a href="/cast/delete/{{$value->id}}" onclick="return confirm('Apakah Anda Yakin Menghapus Data?');" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a> -->
        </form>
        </td> 
    </tr>
    @empty
    <tr>
        <td>Tidak ada data</td>
    </tr>
    @endforelse
  </tbody>
</table>

 @endsection
@extends('master')

@section('title')
Halaman Detail Jenis Produk
@endsection

@section('konten')
    <form action="/minpemesanan/{{$min_pemesanan->id}}" method="post" enctype="multipart/form-data" disabled>
        @csrf
        <label>Minimal Pemesanan :</label><br>
        <input type="text" value="{{$min_pemesanan->min_pemesanan}}" class="form-control" disabled> <br>
        
    </form>

        <a href="/minpemesanan" class="btn btn-secondary btn-sm">Back</a>

 @endsection
 
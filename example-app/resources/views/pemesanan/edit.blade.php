@extends('master')

@section('title')
Halaman Edit Jenis Produk
@endsection

@section('konten')
    <form action="/minpemesanan/{{$min_pemesanan->id}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <label>Jenis Produk :</label><br>
        <input type="text" name="min_pemesanan" value="{{$min_pemesanan->min_pemesanan}}" class="form-control"> <br>
        
        <button type="submit" class="btn btn-primary">Submit</button> <br>

        
    </form>
    
        <a href="/minpemesanan" class="btn btn-secondary btn-sm my-3">Back</a>

 @endsection
 
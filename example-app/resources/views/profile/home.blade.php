@extends('master')

@section('title')
Profile
@endsection

@section('konten')
    <form action="/profile/{{$detailprofile->id}}" method="post" >
        @csrf
        @method('PUT')
        <label>Bio :</label><br>
        <input type="text" name="bio" value="{{$detailprofile->bio}}" class="form-control"> <br>

        <label>Umur :</label><br>
        <input type="number" name="age" value="{{$detailprofile->age}}" class="form-control"> <br>

        
        
        <button type="submit" class="btn btn-primary">Submit</button> <br>

        
    </form>
    
        <a href="/" class="btn btn-secondary btn-sm my-3">Back</a>

 @endsection
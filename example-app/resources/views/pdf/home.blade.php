<!DOCTYPE html>
<html>
<head>
	<title>Data Transaksi</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
 
	<div class="form-control">
		
		<!-- <a href="/pegawai/cetak_pdf" class="btn btn-primary" target="_blank">CETAK PDF</a> -->
		<table class='table table-bordered'>
			<thead>
				<tr>
					<th>Tanggal Pemesanan</th>
					<th>Nama Barang</th>					
					<th>Harga</th>
					<th>Jenis Pembayaran</th>
					<th>Nama Customer</th>
					<th>No Hp Customer</th>
					<th>Email Customer</th>
				</tr>
			</thead>
			<tbody>				
				@foreach($transaksi as $p)
				<tr>
					<td>{{$p->created_at}}</td>
					<td>{{$p->nama_barang}}</td>
					<td>{{$p->harga}}</td>
					<td>{{$p->pembayaran}}</td>
					<td>{{$p->customer_name}}</td>
					<td>{{$p->customer_number}}</td>
					<td>{{$p->customer_email}}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
 
	</div>
 
</body>
</html>